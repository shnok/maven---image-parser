# Weather Image Process
The aim of this project is to extract weather data from forecast preview images provided by [Copernicus Atmosphere Monitoring Service](https://atmosphere.copernicus.eu/themes/custom/ce/logo.svg).

This method is of course as not as accurate as a buying an access to their datasets.
