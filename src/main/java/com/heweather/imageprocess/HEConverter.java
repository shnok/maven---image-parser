package com.heweather.imageprocess;

import org.apache.commons.io.FilenameUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class HEConverter {

    public HEConverter() {

    }
    
    // Convert pdf file to image
    public void convert(String path) throws IOException {
        File file = new File(path);
        try ( PDDocument doc = PDDocument.load(file)) {
            PDFRenderer   renderer = new PDFRenderer(doc);

            PDRectangle cropBox = new PDRectangle();
            cropBox.setUpperRightX(1168);
            cropBox.setUpperRightY(732);
            cropBox.setLowerLeftX(20);
            cropBox.setLowerLeftY(160);
            doc.getPage(0).setCropBox(cropBox);

            BufferedImage image    = renderer.renderImageWithDPI(0, 564.5f);

            String outUrl = String.format("%simages/%s.png", FilenameUtils.getPath(path), FilenameUtils.getBaseName(path));
            System.out.println(outUrl);
            ImageIO.write(image, "PNG", new File(outUrl));
        }
    }
}
