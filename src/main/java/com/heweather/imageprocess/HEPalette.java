package com.heweather.imageprocess;

import java.util.HashMap;
import java.util.Map;

public class HEPalette {

    private static     Map<String, float[][]>   palettes;
    private static     Map<String, Unit> units;

    public static void initPalette() {
        palettes = new HashMap<>();
        units = new HashMap<>();
        units.put("so2", new Unit("so2","ppbv", 64.066f));
        units.put("no2", new Unit("no2","ppbv", 46.0055f));
        units.put("o3", new Unit("o3","ppbv", 48f));
        units.put("co", new Unit("co","ppbv", 28.01f));
        units.put("pm2.5", new Unit("pm2.5","ug/m3", 0));
        units.put("pm10", new Unit("pm10","ug/m3", 0));

        float[][] palette;
        //O3
        palette = new float[13][4];
        palette[0] = new float[]{157,199,233, 5f};
        palette[1] = new float[]{31,157,222, 15f};
        palette[2] =  new float[]{23, 122, 189, 25f};
        palette[3] = new float[]{43, 89, 158, 35f};
        palette[4] = new float[]{45, 146, 114, 45f};
        palette[5] = new float[]{109, 180, 97, 55f};
        palette[6] = new float[]{168, 202, 113, 65f};
        palette[7] = new float[]{254, 245, 160, 75f};
        palette[8] = new float[]{242, 210, 71, 85f};
        palette[9] = new float[]{232, 182, 66, 95f};
        palette[10] = new float[]{219, 135, 68, 110f};
        palette[11] = new float[]{201, 49, 50, 130f};
        palette[12] = new float[]{153, 16, 34, 150f};
        palettes.put("o3", palette);

        //PM10
        palette = new float[11][4];
        palette[0] = new float[]{255,255,255, 15f};
        palette[1] = new float[]{237,151,197, 35f};
        palette[2] =  new float[]{206, 255, 207, 45f};
        palette[3] = new float[]{156, 241, 181, 55f};
        palette[4] = new float[]{88, 190, 160, 70f};
        palette[5] = new float[]{57, 167, 151, 90f};
        palette[6] = new float[]{57, 150, 179, 125f};
        palette[7] = new float[]{23, 111, 174, 175f};
        palette[8] = new float[]{16, 79, 138, 250f};
        palette[9] = new float[]{18, 23, 147, 400f};
        palette[10] = new float[]{44, 0, 69, 750f};
        palettes.put("pm10", palette);

        //PM2.5
        palettes.put("pm2.5", palette);

        //no2
        palette = new float[15][4];
        palette[0] = new float[]{255,255,255, 0f};
        palette[1] = new float[]{157,199,233, 0.015f};
        palette[2] = new float[]{31,157,222, 0.025f};
        palette[3] =  new float[]{23, 122, 189, 0.075f};
        palette[4] = new float[]{43, 89, 158, 0.15f};
        palette[5] = new float[]{45, 146, 114, 0.25f};
        palette[6] = new float[]{109, 180, 97, 0.75f};
        palette[7] = new float[]{168, 202, 113, 1.5f};
        palette[8] = new float[]{254, 245, 160, 2.5f};
        palette[9] = new float[]{242, 210, 71, 7.5f};
        palette[10] = new float[]{232, 182, 66, 12.5f};
        palette[11] = new float[]{219, 135, 68, 17.5f};
        palette[12] = new float[]{201, 49, 50, 35f};
        palette[13] = new float[]{153, 16, 34, 75f};
        palette[14] = new float[]{85, 24, 25, 200f};
        palettes.put("no2", palette);

        //so2
        palette = new float[14][4];
        palette[0] = new float[]{255,255,255, 0f};
        palette[1] = new float[]{157,199,233, 0.025f};
        palette[2] = new float[]{31,157,222, 0.075f};
        palette[3] =  new float[]{23, 122, 189, 0.25f};
        palette[4] = new float[]{43, 89, 158, 0.75f};
        palette[5] = new float[]{45, 146, 114, 1.5f};
        palette[6] = new float[]{109, 180, 97, 3.5f};
        palette[7] = new float[]{168, 202, 113, 7.5f};
        palette[8] = new float[]{254, 245, 160, 15f};
        palette[9] = new float[]{242, 210, 71, 30f};
        palette[10] = new float[]{232, 182, 66, 75f};
        palette[11] = new float[]{219, 135, 68, 150f};
        palette[12] = new float[]{201, 49, 50, 350f};
        palette[13] = new float[]{153, 16, 34, 750f};
        palettes.put("so2", palette);

        //CO
        palette = new float[14][4];
        palette[0] = new float[]{157,199,233, 20};
        palette[1] = new float[]{31,157,222, 50};
        palette[2] =  new float[]{23, 122, 189, 70};
        palette[3] = new float[]{43, 89, 158, 90};
        palette[4] = new float[]{45, 146, 114, 110};
        palette[5] = new float[]{109, 180, 97, 130};
        palette[6] = new float[]{168, 202, 113, 150};
        palette[7] = new float[]{254, 245, 160, 170};
        palette[8] = new float[]{242, 210, 71, 190};
        palette[9] = new float[]{232, 182, 66, 350};
        palette[10] = new float[]{219, 135, 68, 750};
        palette[11] = new float[]{201, 49, 50, 1500};
        palette[12] = new float[]{153, 16, 34, 2500};
        palette[13] = new float[]{85, 24, 25, 3000};
        palettes.put("co", palette);

    }

    public static float[][] getPalette(String variable) {
        return palettes.get(variable.toLowerCase());
    }
    public static Unit getUnit(String variable) {
        return units.get(variable.toLowerCase());
    }
}
