package com.heweather.imageprocess;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HEParser {

    private int       imgWidth;
    private int       imgHeight;
    private String    variable;
    private Unit      unit;
    private float[][] data;
    private String    path;

    private int[][]     paletteId;  // for debug purpose
    private List<int[]> points = new ArrayList<>(); // for debug purpose

    public HEParser(String variable, String path) {
        this.unit = HEPalette.getUnit(variable);
        this.variable = variable;
        this.path = path;
    }

    // int rgb to r-g-b-a values
    private int[] rgb(int argb) {
        int b = (argb) & 0xFF;
        int g = (argb >> 8) & 0xFF;
        int r = (argb >> 16) & 0xFF;
        int a = (argb >> 24) & 0xFF;

        return new int[]{ r, g, b };
    }

    // RGB comparison method
    private int colorDiff(int[] c1, float[] c2) {
        return (int) Math.sqrt((c1[0] - c2[0]) * (c1[0] - c2[0]) + (c1[1] - c2[1]) * (c1[1] - c2[1]) + (c1[2] - c2[2]) * (c1[2] - c2[2]));
    }

    // Get the distance between colors
    private int matchColor(float[][] rgbPalette, int[] color) {
        int oldDiff   = colorDiff(color, rgbPalette[0]);
        int bestMatch = 0;

        for ( int i = 1; i < rgbPalette.length; i++ ) {
            int newDiff = colorDiff(color, rgbPalette[i]);
            if ( newDiff < oldDiff ) {
                oldDiff = newDiff;
                bestMatch = i;
            }
        }
        return bestMatch;
    }

    // Compare colors with palette
    private float[][] parseRGBs(int[] rgbData) {
        float[][] palette = HEPalette.getPalette(variable);
        float[][] vals    = new float[imgHeight][imgWidth];
        paletteId = new int[imgHeight][imgWidth]; // for debug purpose

        int index        = 0;
        int paletteIndex = 0;
        int prev         = 0;
        for ( int y = 0; y < imgHeight; y++ ) {
            for ( int x = 0; x < imgWidth; x++ ) {
                // Check if the new pixel color is different
                if ( rgbData[index] != prev ) {
                    paletteIndex = matchColor(palette, rgb(rgbData[index]));
                    prev = rgbData[index];
                }

                paletteId[y][x] = paletteIndex; // for debug purpose
                vals[y][x] = palette[paletteIndex][3];
                index++;
            }
        }
        return vals;
    }

    // Calculate the array index from coordinates
    private int[] calcIndex(float lat, float lon) {
        int   height = imgHeight - 1;
        int   width  = imgWidth - 1;
        int[] ids    = new int[2];

        ids[0] = (int) (height - Math.floor((lat + 90) / 180 * height));
        ids[1] = (int) Math.floor((lon + 180) / 360 * width);
        points.add(ids); //for debug only

        return ids;
    }

    // Parse image to values
    public void readImage() throws IOException {
        File          f   = new File(path);
        BufferedImage img = ImageIO.read(f);

        this.imgHeight = img.getHeight();
        this.imgWidth = img.getWidth();

        int[] rgbVal = new int[imgWidth * imgHeight];
        img.getRGB(0, 0, imgWidth, imgHeight, rgbVal, 0, imgWidth);
        data = parseRGBs(rgbVal);
    }

    // convert units
    private float convertUnit(float value, Float tmp, Float press) {
        // ppbv to ug/m3
        if ( unit.getUnit().equals("ppbv") && tmp != null && press != null) {
            float ppb         = value;
            float molWeight   = unit.getMolWeight();
            float stdTemp     = 273.15f;
            float stdPressure = 1013.25f;

            float measuredPressure = press; //1013.25f
            float measuredTmp      = tmp;

            value = (float) ((ppb * molWeight * stdTemp * measuredPressure) / (22.4136 * (measuredTmp + stdTemp) * stdPressure));

            // ug/m3 to g/m3
            if ( unit.getName().equals("co") ) {
                value = value * 1e-6f;
            }
        }
        return value;
    }

    // Get value at coordinates
    public float getValueAt(float lat, float lon) {
        return getValueAt(lat, lon, null, null);
    }

    public float getValueAt(float lat, float lon, Float tmp, Float press) {
        int[] ids   = calcIndex(lat, lon);
        float value = convertUnit(data[ids[0]][ids[1]], tmp, press);
        //System.out.println(String.format("[%.2f,%.2f] %s: %s %s", lat, lon, variable, value, unit.getName().equals("co") ? "g/m3" : "ug/m3"));
        return value;
    }

    // DEBUG: Draw an image to check the data
    public void debugData() {
        BufferedImage bufferedImage = new BufferedImage(imgWidth, imgHeight, BufferedImage.TYPE_INT_RGB);

        Graphics2D g = bufferedImage.createGraphics();
        g.setBackground(Color.black);

        float[][] rgbPalette = HEPalette.getPalette(variable);

        // draw image
        for ( int y = 0; y < imgHeight; y++ ) {
            for ( int x = 0; x < imgWidth; x++ ) {
                float[] col      = rgbPalette[paletteId[y][x]];
                Color   newColor = new Color((int) col[0], (int) col[1], (int) col[2]);
                g.setColor(newColor);
                g.fillRect(x, y, 1, 1);
            }
        }

        // draw checked values
        points.forEach((loc) -> {
            g.setColor(Color.magenta);
            g.fillRect(loc[1], loc[0], 10, 10);
        });

        try {
            ImageIO.write(bufferedImage, "png", new File("store/debug.png"));
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

}
