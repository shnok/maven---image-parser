package com.heweather.imageprocess;

public class Unit {
    private String name;
    private String unit;
    private float molWeight;

    public Unit(String name, String unit, float molWeight) {
        this.name = name;
        this.unit = unit;
        this.molWeight = molWeight;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public float getMolWeight() {
        return molWeight;
    }

    public void setMolWeight(float molWeight) {
        this.molWeight = molWeight;
    }
}
