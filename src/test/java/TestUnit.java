import com.heweather.imageprocess.HEConverter;
import com.heweather.imageprocess.HEPalette;
import com.heweather.imageprocess.HEParser;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;
import java.nio.file.Path;

public class TestUnit {
    @Test
    public void testConvert() {
        try {
            HEConverter converter = new HEConverter();
            converter.convert("store/o3.pdf");
            converter.convert("store/co.pdf");
            converter.convert("store/no2.pdf");
            converter.convert("store/so2.pdf");
            converter.convert("store/pm10.pdf");
            converter.convert("store/pm25.pdf");
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    @Test
    public void testReading() {
        HEPalette.initPalette();
        try {
            System.out.println("O3");
            HEParser parser = new HEParser("o3", "store/images/o3.png");
            parser.readImage();
            parser.getValueAt(-13.55f, 48.90f, 20f , 1013.20f);

            System.out.println("CO");
            parser = new HEParser("co", "store/images/co.png");
            parser.readImage();
            parser.getValueAt(-13.55f, 48.90f, 20f , 1013.20f);

            System.out.println("SO2");
            parser = new HEParser("so2", "store/images/so2.png");
            parser.readImage();
            parser.getValueAt(-13.55f, 48.90f, 20f , 1013.20f);

            System.out.println("PM10");
            parser = new HEParser("pm10", "store/images/pm10.png");
            parser.readImage();
            parser.getValueAt(-13.55f, 48.90f);

            System.out.println("PM2.5");
            parser = new HEParser("pm2.5", "store/images/pm25.png");
            parser.readImage();
            parser.getValueAt(-13.55f, 48.90f);

            System.out.println("NO2");
            parser = new HEParser("no2", "store/images/no2.png");
            parser.readImage();
            parser.getValueAt(-13.55f, 48.90f, 20f , 1013.20f);

            // print image + measured points
            parser.debugData();

        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
